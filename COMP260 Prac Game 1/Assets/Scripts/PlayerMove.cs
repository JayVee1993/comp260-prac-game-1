﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
	public Vector2 move;
	public string horizontalAxisString = "Horizontal1";
	public string verticalAxisString = "Vertical1";
	// Use this for initialization
	void Start () {
	
	}

	public float maxSpeed = 5.0f;

	// Update is called once per frame
	void Update () {
		// get the input values
		Vector2 direction;
		direction.x = Input.GetAxis(horizontalAxisString);
		direction.y = Input.GetAxis(verticalAxisString);

		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);
	}
		
}
