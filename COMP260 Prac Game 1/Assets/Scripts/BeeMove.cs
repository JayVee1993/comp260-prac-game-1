﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {
	public float speed = 4.0f;		 // meters per second
	public float turnSpeed = 180.0f; // degrees per second
	public Transform target;
	public Transform target2;

	public Vector2 heading = Vector3.right;

	// Use this for initialization
	void Start () {
	
	}

	void OnDrawGizmos(){
		// draw heading vector is red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;

		Gizmos.DrawRay(transform.position, direction);
		Gizmos.DrawRay(transform.position, direction2);

	}

	// Update is called once per frame
	void Update () {
		// get the vector from the bee to the target
		Vector2 direction = target.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;

		Vector2 Close;
		if(direction.magnitude < direction2.magnitude )
		{
			Close = direction;
		}else{
			Close = direction2;
		}

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (Close.IsOnLeft (heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate (angle);
		} else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate (heading * speed * Time.deltaTime);
	}
}
